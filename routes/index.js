const express = require('express')
const router = express.Router()

// Main router
router.use('/elastic', require('./elastic.js'))

router.get('/', (req, res) => res.send("Hi"))

module.exports = router