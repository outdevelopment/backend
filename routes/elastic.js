const express = require('express')
const router = express.Router()
const elastic_client = require('../elastic_client')
let {random_password} = require('../utils')

function obj_to_list(obj){
	let _list = []
	for (var key in obj) {
		_list.push(obj[key])
	}
	return _list
}

// GET user or List all users
router.get('/user/:username?', (req, res, next) => {
	console.log("YAY")
	let username = req.params.username
	elastic_client.security.getUser({username: username || ''})
		.then(({body}) => {
			res.json(obj_to_list(body))
		})
		.catch(error => {
			console.log(error)
			res.json([])
		})
})

// Enable user
router.put('/user/:username/enable', (req, res, next) => {
	elastic_client.security.enableUser({username: req.params.username || ''})
		.then(result => {
			res.json({enabled:true, ...result.body})
		})
		.catch(error => next(error))
})

// Disable user
router.put('/user/:username/disable', (req, res, next) => {
	elastic_client.security.disableUser({username: req.params.username || ''})
		.then(result => {
			res.json({enabled:true, ...result.body})
		})
		.catch(error => next(error))
})

// DELETE user
router.delete('/user/:username', (req, res, next) => {
	elastic_client.security.deleteUser({username: req.params.username})
		.then(result => {
			res.json(result.body)
		})
		.catch(error => next(error))
})

// PUT (Create / Update) User
router.put('/user/:username', (req, res, next)=>{
	const username = req.params.username;
	console.log("BODY:" , req.body)
	if(username) {
		let user = { }

		// Fetch current user fieds
		elastic_client.security.getUser({username})
			.then(({body}) => {
				// merge changes to user
				user.body = body[username]
				for (var _key in req.body){
					user.body[_key] = req.body[_key]
				}

				elastic_client.security.putUser({username, body:user.body})
					.then(result => {
						res.json(result)
					})
					.catch(error => next(error))
			})
			.catch(error => {
				// user not found - create user
				user = elastic_client.models.User(username)
				console.log(">>", user)
				for (var _key in req.body){
					user.body[_key] = req.body[_key]
				}
				// default random 6-digits password
				if(!user.body.password)
					user.body.password = random_password(6)
				
				elastic_client.security.putUser({username, body:user.body})
				.then(result => {
					res.json(result)
				})
				.catch(error => next(error))
			})
	} else {
		res.status(400).send({error: 'please provide a username'})
	}
})

// GET user roles or List all users roles
router.get('/role/:role?', (req, res, next) => {
	elastic_client.security.getRole({name: req.params.role || ''})
		.then(result => {
			res.json(result.body)
		})
		.catch(error => next(error))
})

module.exports = router