const { Client } = require('@elastic/elasticsearch')

// AWS => 35.232.124.123:9200
const elastic_server_host = process.env.ELASTIC_SERVER_HOST || '35.232.124.123'
const elastic_server_port = process.env.ELASTIC_SERVER_PORT || '9200'
const elastic_user = process.env.ELASTIC_USER || 'snir'
const elastic_password = process.env.ELASTIC_PASSWORD || 'password'

// Instantiate elastic client
const client = new Client({
    node: 'http://' + elastic_server_host + ':' + elastic_server_port ,
    auth: {
        username: elastic_user,
        password: elastic_password
    },
    sniffOnStart: true,
    keepAlive: true
})

client.security.getUser().then(() => console.log("Connected to Elastic")).catch(error => console.error(error))

// Models
client.models = {
    User: function(username=''){
        return new Object({
            username: username,
            refresh: '',
            body: {
                roles: [],
                full_name: '',
                email: '',
                metadata: {}
            }
        })
    }
}

module.exports = client
