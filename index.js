const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')

// Configurations
const port = 8000

// Integrate the main router
app.use(cors())
app.use(bodyParser.json())
app.use('/', require('./routes'))

// Simple Error Handler
app.use((error, req, res, next) => {
    console.error(error)
    res.status(500).send({error})
})

// Listen
app.listen(port, () => {
    console.log('API Server is up and running')
})